\documentclass[serif,mathserif]{beamer}

\usepackage{beamerthemesplit}

\usetheme{default}
\useoutertheme{default}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{color}
\usepackage{epsfig}
\usepackage{marvosym}
\usepackage{texnansi}
\usepackage{verbatim}
\usepackage{xspace}
\usepackage{listings}

\lstset{basicstyle=\ttfamily,
	showstringspaces=false,
}

% Make this a 16:9 presentation
%\setlength{\paperwidth}{171 mm}
%\setlength{\paperheight}{96 mm}
%\setlength{\textwidth}{151 mm}
%\setlength{\textheight}{86 mm}

% Set sans-serif font.
\renewcommand\sfdefault{phv}
\renewcommand\familydefault{\sfdefault}

% Define some colours.
\definecolor{myred}{rgb}{0.53,0.01,0}
\definecolor{mygrey}{rgb}{0.4,0.4,0.4}

% Make a nice gradient as background.
\setbeamertemplate{background canvas}[vertical shading]
[bottom=black, middle=myred, top=myred]

% Highlight elements in some sort of grey.
\setbeamercolor{structure}{fg=mygrey}
\setbeamercolor{normal text}{bg=black, fg=white}

% Use round bullets in lists.
\setbeamertemplate{items}[circle]

% Use bigger fonts for titles.
\setbeamerfont{title}{size=\Huge}
\setbeamerfont{frametitle}{size=\large}
\setbeamertemplate{frametitle}[default]

% Don't clutter the pages with useless navigations.
\setbeamertemplate{navigation symbols}{}

% Author information.
\author[Timo Eissler]{Timo Eissler}
\institute{IPFire Project}

% The title of the presentation.
\title{IPFire VLAN Support}
%\subtitle{The new IPFire build system}

\date{September 20th, 2014}

\newcommand{\spacer}{\vspace{4 mm}}

\newcommand{\screenshot}[1]{\centerline{%
    \includegraphics[width=\textwidth,transparent]{#1}}}

\begin{document}
	\maketitle

	%\section*{Outline}
	%\frame{\tableofcontents}

	\section{Introduction}

	\subsection{What are VLANs?}
	\frame{
		\frametitle{What are VLANs?}

		\begin{center}
			Virtual networks within your switch or your physical network.
		\end{center}
	}

	\subsection{VLAN Introduction}
	\frame{
		\begin{itemize}
			\item VLANs work on OSI Layer 2 (Data Link Layer)
			\item VLANs are isolated from each other
			\item Can reside in a single switch or can be spread over
				multiple switches in your network
			\item Adds the 802.1Q header to the Ethernet frame
		\end{itemize}
	}
	\frame{
		\frametitle{Tagged and untagged frames}

		\begin{block}{Tagged frames}
			Tagged frames have a VLAN header in the Ethernet frame and so
			receiving devices know to which VLAN a frame belongs to.
		\end{block}

		\begin{block}{Untagged frames}
			Untagged frames have no VLAN header in the Ethernet frame and so
			the receiving devices have to decide to which VLAN a frame belongs to.
			Switches do this by using the default VLAN ID (also known as PVID),
			which is set by the administrator of the switch for the
			corresponding port.
		\end{block}
	}
	\frame{
		\frametitle{Access mode port}

		\begin{itemize}
			\item Used for client devices
			\item Sends and accepts only untagged frames
			\item Port can only be member of one VLAN
			\item Connected devices don't need to be able to handle VLANs
		\end{itemize}
	}
	\frame{
		\frametitle{Trunk mode port}

		\begin{itemize}
			\item Uses tagged and untagged frames
			\item Untagged frames belong to default VLAN (PVID)
			\item Port can be a untagged member of one VLAN
			\item Port can be a tagged member of zero or more VLANs
			\item Sends and accepts only frames which belong to a VLAN the
				port is a member of
			\item Connected devices need to be able to handle VLANs
		\end{itemize}
	}

	\subsection{Why VLANs?}
	\frame{
		VLANs can be used to:

		\begin{itemize}
			\item seperate and manage networks
			\item devices can be grouped together even if they are not on the same switch
			\item optimize performance for specific scenarios like VoIP, SAN, \dots
			\item minimize broadcast domains (with switches only on layer 2 (MAC))
			\item minimize hardware costs
			\item \dots
		\end{itemize}
	}

	\subsection{Ok, but why VLANs on my IPFire?}
	\frame{
		There are several reasons for this:

		\begin{itemize}
			\item you already have your networks seperated with VLANs and your 
				corresponding IPFire networks are not within the default VLAN (PVID)
			\item multiple isolated networks (maybe on a single switch)
			\item you want more networks than your IPFire has NICs
			\item and maybe many more\dots
		\end{itemize}
	}

	\section{Configuration}

	\subsection{Overview}
	\frame{
		\frametitle{origin state}

		\begin{figure}
			\screenshot{res/IPFire_network_without_VLANs.png}
		\end{figure}
	}
	\frame{
		\frametitle{desired state}

		\begin{figure}
			\screenshot{res/IPFire_network_with_VLANs.png}
		\end{figure}
	}
	\frame{
		\begin{block}{Steps}
			\begin{enumerate}
				\item Configure VLANs on the switch
				\item Configure VLANs on IPFire
				\item	Change IPFire network configuration
				\item Restart network or reboot
			\end{enumerate}
		\end{block}
	}

	\subsection{Configure VLANs on the switch}
	\frame{
		\begin{itemize}
			\item Port mode: Trunk
			\item Default VLAN of port: VLAN ID 1 untagged
			\item VLAN ID for blue network: VLAN ID 300 tagged
			\item VLAN ID for orange network: VLAN ID 400 tagged
		\end{itemize}
	}

	\subsection{Configure VLANs on IPFire}
	\begin{frame}[fragile]
		\frametitle{/var/ipfire/ethernet/vlans}

		\begin{lstlisting}[language=bash]
			GREEN_PARENT_DEV=
			GREEN_VLAN_ID=
			GREEN_MAC_ADDRESS=
			BLUE_PARENT_DEV=green0
			BLUE_VLAN_ID=300
			BLUE_MAC_ADDRESS=
			ORANGE_PARENT_DEV=green0
			ORANGE_VLAN_ID=400
			ORANGE_MAC_ADDRESS=
		\end{lstlisting}
	\end{frame}

	\subsection{Network Configuration}
	\begin{frame}[fragile]
		\frametitle{/var/ipfire/ethernet/settings}

		\begin{lstlisting}[language=bash]
			CONFIG_TYPE=4
			...
			GREEN_MACADDR=52:54:00:67:58:de
			GREEN_DESCRIPTION='"virtio: virtio_net"'
			GREEN_DRIVER=virtio_net
			...
			BLUE_DEV=blue0
			BLUE_MACADDR=52:54:00:67:58:de
			BLUE_DESCRIPTION='"virtio: virtio_net"'
			BLUE_DRIVER=virtio_net
			BLUE_ADDRESS=192.168.100.30
			BLUE_NETMASK=255.255.255.0
			BLUE_NETADDRESS=192.168.100.0
			BLUE_BROADCAST=192.168.100.255
		\end{lstlisting}
	\end{frame}
	\begin{frame}[fragile]
		\frametitle{/var/ipfire/ethernet/settings}

		\begin{lstlisting}[language=bash]
			ORANGE_DEV=orange0
			ORANGE_MACADDR=52:54:00:67:58:de
			ORANGE_DESCRIPTION='"virtio: virtio_net"'
			ORANGE_DRIVER=virtio_net
			ORANGE_ADDRESS=192.168.150.30
			ORANGE_NETMASK=255.255.255.0
			ORANGE_NETADDRESS=192.168.150.0
			ORANGE_BROADCAST=192.168.150.255
		\end{lstlisting}
	\end{frame}

	\section{Demo}

	\subsection{Demo}
	\frame{
		\begin{center}
			\Huge{DEMO}
			\spacer
		\end{center}
	}

	\section{The end}

	\subsection{Conclusion}
	\frame{
		\frametitle{Conclusion}

		\begin{center}
			VLANs are an easy way to manage and improve your networks for your specific needs.
		\end{center}
	}

	\subsection{The end}
	\frame{
		\begin{center}
			\Huge{THE END}
			\spacer
		\end{center}

		\begin{center}
			If you want to know more or need more information look at
			\spacer
			\url{http://wiki.ipfire.org/en/optimization/vlan/start}
		\end{center}

		\begin{center}
			\url{timo.eissler@ipfire.org}
		\end{center}
	}

\end{document}

% vim:set noet:
